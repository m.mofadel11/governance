This is an overview of tasks that Vincent performs as part of keys.openpgp.org maintenance and operations.
Those tasks are at the moment a fairly unformalized process.

code:
* basic code maintenance

maintenance:
* respond to issues, figure out relevant bugs.
* respond to support@keys.openpgp.org emails. this is roughly one per week, most are unimportant.
* review and merge MRs. the difficult part here is only the decision-making.
* respond in Hagrid Matrix room

infrastructure:
* monitor uptime (using uptimerobot)
* monitor resources (currently a non-issue due to low load)
* monitor backups (made and pulled daily, mostly non-issue but gpg key expired recently and I hadn't noticed)
* resolve infrastructure issues (e.g. recently had to adjust TLS config)
* resolve issues with hoster eclips.is when necessary (e.g. IPv6 connectivity, but had no issues in a long time)
* perform software upgrades on servers (prod, mail, wkd gateway)
* manage hosting (non-issue so far, eclips.is)
* manage DNS (Phil)

other:
* keep dialog with interested parties
* participate in meetings

special:
* small but constant piece of mindshare
