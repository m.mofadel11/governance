Members of the KOO voting body--

We need to select the initial board for keys.openpgp.org (a.k.a. KOO).
This message outlines the process and schedule to make that happen.

# Summary

 - Today (October WWW, 2022) we open the self-nomination process.  Any of
   the voting members can present themselves as a candidate for the
   KOO Board.
   
 - On October XXX (14 days afer WWW) we will begin the voting process.
   Each member of the voting body can indicate which candidates they
   approve of as a potential Board member.
 
 - On November YYY (14 days after XXX), the election organizer (that's
   me, Patrick) will announce the end of voting.
   
 - No later than November ZZZ (7 days after YYY), I will confirm the
   five most-approved candidates to constitute the new Board.

 - The new Board assumes its duties on December 1, 2022.

Please read `electoral-process.md` and associated material at
https://gitlab.com/hagrid-keyserver/bootstrap-committee if you're
interested in more detail about the reasoning behind this process.

# How Do I Self-Nominate?

Any voting member can self-nominate between now and October XXX-1,
23:59 UTC by sending a cryptographically-signed message to this
mailing list stating their intention to stand as a Board candidate.

As a candidate, please identify yourself with a reasonable, unique
UTF-8 string (ideally from an OpenPGP User ID on your OpenPGP
certificate).

This message could be as simple as:

    I hereby announce myself as a 2022 candidate for the
    Board of keys.openpgp.org under the name "Alice Jones"

(We all know it's funny to think of arbitrary names, but please just
use a name you're known by in this community.  And keep it sensibly
short, to avoid line-wrapping issues on the ballot.)






-----

The rest of this message is a commitment to the mechanisms we will use
to process this particular election.  You don't need to read it now,
you will be reminded of the details as we progress.  This is our first
go at this, so please bear with us as we work through it.


# How Will I Vote?

The election organizer will send out detailed instructions on voting
on October XXX, including the list of valid candidates.  The process
is described below:

You'll vote with a text file.  The content and the author of any vote
will be public.  At some point in the text file you include a magic
line with only the following string:

    === 2022 keys.openpgp.org Board approvals ===

(this magic line includes the equals signs, is case-insensitive, and
ignores leading and trailing whitespace)

Below that line, you write the name of each candidate that you approve
of, one per line.  We'll strip leading and trailing whitespace, but
please try to keep everything else the way the candidate announced
themselves so we don't have to play full-blown Unicode normalization
games.  We'll ignore any lines that don't match one of the declared
candidates.

You can approve of ("vote for") every and any candidate that you want
to, by listing them on the ballot.  There is no limit.

You'll cast your ballot by sending it, cryptographically signed, to
the election organizer (that's me!).  During this voting period, I'll
post regular summaries of who I have received readable ballots from to
this list.  If i can't make sense of your ballot, I'll let you know.

If you want to enhance the ability to break ties in the election (see
"Breaking Ties" below), you can also instead send me just the OpenPGP
signature of your ballot before the election concludes.  I'll publish
all the signed ballots (and, for those who submitted them, dangling
signatures) at the end of the election.

If you submitted a dangling signature, you'll have 24 hours after the
end of the election to send the ballot itself, which I'll then confirm
against the signature.  Note that if you send a dangling signature, I
cannot verify that your ballot is readable until after it is too late
for you to correct it.

If you later send me another ballot, I will use the last one that I
received before the voting ends, sorted based on the OpenPGP timestamp
in the signature.  A later ballot completely replaces any approvals
from the earlier ballots.

24 hours after the voting ends, I'll publish all the signed ballots
and the rank order of selected candidates that I plan to use for the
process of filling the Board.

# How Is The Board Filled?

There are five Board seats.  Each candidate that received at least
five approvals is eligible to fill a Board seat.  We will fill the
seats starting from the most-approved candidates.

The ordered list of candidates ranked by number of approvals, with
ties broken as described in "Breaking Ties" below.

If any selected candidate declines to join the Board, they will be
skipped and the next candidate on the list will be offered the seat.

Note that some candidates *should* decline a Board seat when offered.
In addition to people who find they need to bow out for logistical
reasons, we want a diverse board, because the community is diverse.
If the Board looks like it's starting to be lots of your team already,
you should probably consider declining this time around to make sure
other teams are also represented.  The KOO Constitution (see
`constitution.md`) explicitly says:

> * When offered a seat on a new Board, a candidate is expected to
>   decline the seat if they feel their affiliations are already
>   well-represented on the Board being assembled.

Note that in an asynchronous, remote election, each selected candidate
may need some time to accept or reject a seat, so this might take some
time.  If you are a candidate, you'll have at least 24 hours to
confirm before I move on, but please watch your mail during this
crucial week if you're a candidate who received five or more
approvals.  If you are offered a seat and you do not send a
cryptographically-signed acceptance 24 hours after the offer, you will
be skipped.

If you are selected and want to decline, you can speed up the process
for everyone else by sending me a cryptographically-signed rejection.

On November ZZZ, along with the names on the newly assembled Board,
I'll publish a simple textual summary of the process by which we
arrived at the result.

The Board begins its new term on December 1, 2022.

# Breaking Ties

One problem with approval voting with a relatively small electorate is
that sometimes there are ties.  See `tie-breaking.md` for more details
and rationale, but for this election, we will do the following:

For each valid and contributing ballot B, at most one per voting
member:

 - Verify the signature of B
 - let T be the bytestream representing the OpenPGP-canonical textual
   form of B (without the signature)
 - let H(T) = SHA2_256("KOO Board 2022 sort order:" || T)

Sort the ballots B by H(T) (hash 00...0 comes first, hash ff...f comes
last), and concatenate the ballots themselves into one large text
document X.

Where "hex" means "lower-case hexadecimal ASCII representation",
calculate a seed string:

    S = hex(SHA2_256(X))

For a candidate identified by textual label N, each candidate gets a
tie-breaking position:

    Z(N) = SHA2_256(S || N)

If two candidates A and B have the same number of approvals, the one
with a lower Z(N) will be earlier than the other in the ordered list.

# Conclusion

If you read this far, thanks for the interest in the details!  If you
have concerns, suggestions, or comments, please follow up!


# List Of All Voting Members For 2022 KOO Board Election


- Daniel Kahn Gillmor
    C29F8A0C01F35E34D816AA5CE092EB3A5CA10DBA
- Vincent Breitmoser
    D4AB192964F76A7F8F8A9B357BD18320DEADFA11
- Lars Wirzenius
    F3E15038639FCE77094C5EA12C2FDC23E02FE52A
- Patrick Brunschwig
    4F9F89F5505AC1D1A260631CDB1187B9DD5F693B

etc...
